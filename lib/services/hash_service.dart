import 'dart:convert';
import 'package:crypto/crypto.dart';

class HashService {
  static String hashText(String text) {
    List<int> bytes = utf8.encode(text);
    Digest digest = sha256.convert(bytes);
    return digest.toString();
  }
}
