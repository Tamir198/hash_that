import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hash_that/widgets/hash_component.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(EasyLocalization(
    supportedLocales: const [Locale('en', 'US'), Locale('he', 'IL')],
    path: 'assets/translations',
    fallbackLocale: const Locale('en', 'US'),
    child: const App(),
  ));
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.amberAccent),
        useMaterial3: true,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor:Theme.of(context).colorScheme.primary,
          title: const Text('app_title').tr(),
        ),
        body: const HashComponent(), // Your HashComponent widget
      ),
    );
  }
}
